def mae_first_last_clicks(pred, real):
    """
    Mean Absolute Error of first and last real and predicted clicks.

    :returns: Two MAE.
    """
    flag_pred = True
    flag_real = True

    r_first_pred = None
    r_first_real = None
    r_last_pred = None
    r_last_real = None

    for i in xrange(len(pred)):
        if (flag_pred and pred[i] == 1):
            r_first_pred = i
            flag_pred = False
        if (flag_real and real[i] == 1):
            r_first_real = i
            flag_real = False

        if ((not flag_pred) and pred[i] == 1):
            r_last_pred = i
        if ((not flag_real) and real[i] == 1):
            r_last_real = i

    if (r_first_real == None):
        return None, (r_first_pred, r_last_pred)

    if (r_first_pred == None):
        return None, (r_first_real, r_last_real)

    return abs(r_first_pred - r_first_real), abs(r_last_pred - r_last_real)

print(mae_first_last_clicks([1,0,1], [0,0,1]))
from pyclick.utils.YandexPersonalizedChallengeParser import YandexPersonalizedChallengeParser
import numpy as np

from pyclick.click_models.CCM import CCM
from pyclick.click_models.DBN import DBN

def simulating_user_clicks(click_model, query_session):
    """
   Simulates search sessions.

   :returns: Vector of simulated clicks.
   """
    simulated_clicks = []

    session_len = len(query_session.web_results)
    for i in np.arange(session_len):
        query_session.web_results[i].click = 0


    for i in np.arange(session_len):
        P_r = click_model.get_conditional_click_probs(query_session)
        p = P_r[i]
        simulated_click = np.random.binomial(1, p, 1)[0]
        if (simulated_click == 1):
            query_session.web_results[i].click = 1

        simulated_clicks.append(simulated_click)


    return simulated_clicks


search_sessions = YandexPersonalizedChallengeParser().parse("/Users/Stepan/Desktop/test_data/test.txt", 100)


click_model = globals()["DBN"]()

query_session = search_sessions[4]

print(query_session)

print(simulating_user_clicks(click_model, query_session))
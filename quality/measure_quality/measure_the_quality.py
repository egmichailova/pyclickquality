import os
import time
import numpy as np

from pyclick.click_models.CCM import CCM
from pyclick.click_models.DBN import DBN
from pyclick.click_models.task_centric.TaskCentricSearchSession import TaskCentricSearchSession
from pyclick.search_session.SearchResult import SearchResult


def binary_search(f, query):
    """
      Search query and JSON in file f.

      :returns: line with query and JSON or False.
      """
    r = os.fstat(f.fileno()).st_size
    l = 0
    x = int(query)

    while ((r - l) > 1):
        query = (r + l) >> 1

        f.seek(query)
        while f.read(1) != '\n':
            pass

        line = f.readline()
        data = line.split("\t")

        elem = int(data[0])

        if elem > x:
            r = query
        if elem < x:
            l = query

        if (elem == x):
            return line

    return False


def simulating_user_clicks(click_model, query_session):
    """
   Simulates search sessions.

   :returns: Vector of simulated clicks.
   """
    simulated_clicks = []

    session_len = len(query_session.web_results)
    for i in range(session_len):
        query_session.web_results[i].click = 0

    for i in np.arange(session_len):
        P_r = click_model.get_conditional_click_probs(query_session)
        p = P_r[i]
        simulated_click = np.random.binomial(1, p, 1)[0]
        if (simulated_click == 1):
            query_session.web_results[i].click = 1

        simulated_clicks.append(simulated_click)

    return simulated_clicks


def first_simple_simulator(query_session):
    """
   Always simulate no clicks.

   :returns: Vector of simulated clicks.
   """
    session_len = len(query_session.web_results)
    simulated_clicks = [0] * session_len

    return simulated_clicks


def second_simple_simulator(query_session):
    """
   Always simulate click on first position.

   :returns: Vector of simulated clicks.
   """
    session_len = len(query_session.web_results)
    simulated_clicks = [0] * session_len

    simulated_clicks[0] = 1

    return simulated_clicks


def third_simple_simulator(query_session):
    """
   Random choice between first and second.

   :returns: Vector of simulated clicks.
   """
    r = np.random.random()

    if (r < 0.5):
        return first_simple_simulator(query_session)
    else:
        return second_simple_simulator(query_session)


def mae_first_last_clicks(pred, real):
    """
    Mean Absolute Error of first and last real and predicted clicks.

    :returns: Two MAE.
    """
    flag_pred = True
    flag_real = True

    r_first_pred = None
    r_first_real = None
    r_last_pred = None
    r_last_real = None

    for i in range(len(pred)):
        if (flag_pred and pred[i] == 1):
            r_first_pred = i + 1
            flag_pred = False
        if (flag_real and real[i] == 1):
            r_first_real = i + 1
            flag_real = False

        if ((not flag_pred) and pred[i] == 1):
            r_last_pred = i + 1
        if ((not flag_real) and real[i] == 1):
            r_last_real = i + 1

    if (r_first_real == None):
        return None, (r_first_pred, r_last_pred)

    if (r_first_pred == None):
        return None, (r_first_real, r_last_real)

    return abs(r_first_pred - r_first_real), abs(r_last_pred - r_last_real)


def mean_squared_error(pred, real):
    """
    Mean Squared Error of real and predicted clicks.

    :returns: MSE.
    """
    pred = np.array(pred)
    real = np.array(real)

    return ((pred - real) ** 2).mean(axis=0)


def get_result_for(number_of_simulator, real_clicks):
    if (number_of_simulator == 1):
        pred_clicks = first_simple_simulator(session)
    elif (number_of_simulator == 2):
        pred_clicks = second_simple_simulator(session)
    elif (number_of_simulator == 3):
        pred_clicks = third_simple_simulator(session)
    else:
        raise Exception

    fst, snd = mae_first_last_clicks(pred_clicks, real_clicks)
    mse = mean_squared_error(pred_clicks, real_clicks)

    result = query + '\t' + str(fst) + '\t' + str(snd) + '\t' + str(mse) + '\n'

    return result


class SessionsIterator(object):
    def __init__(self, fname, session_max=None):
        self.f = open(fname, 'r')
        self.session_max = session_max
        self.prev_line = 'start'
        self.flag = False
        self.line_number = 0

        while (self.line_number < -1):
            read_line = self.f.readline()
            self.line_number += 1

    def __iter__(self):
        return self

    def next(self):
        sessions = []

        while True:
            if (self.flag):
                line = self.prev_line
                self.flag = False
            else:
                line = self.f.readline()
                self.line_number += 1

                if (self.line_number % 100000 == 0):
                    print self.line_number

            entry_array = line.strip().split("\t")

            if len(entry_array) == 4 and entry_array[1] == 'M':
                pass

            elif len(entry_array) >= 7 and (entry_array[2] == 'Q' or entry_array[2] == 'T'):

                if self.session_max and len(sessions) >= self.session_max:
                    self.prev_line = line
                    break

                task = entry_array[0]
                serp = entry_array[3]
                query = entry_array[4]
                urls_domains = entry_array[6:]
                session = TaskCentricSearchSession(task, query)

                results = []
                for url_domain in urls_domains:
                    result = url_domain.strip().split(',')[0]
                    url_domain = SearchResult(result, 0)
                    results.append(result)

                    session.web_results.append(url_domain)

                sessions.append(session)

            elif len(entry_array) == 5 and entry_array[2] == 'C':
                if entry_array[0] == task and entry_array[3] == serp:
                    clicked_result = entry_array[4]
                    if clicked_result in results:
                        index = results.index(clicked_result)
                        session.web_results[index].click = 1
            else:
                if not line:
                    if (sessions != []):
                        self.flag = False
                        return sessions

                    raise StopIteration

        if (sessions != []):
            self.flag = True
            return sessions

        raise StopIteration


# click_model = globals()["DBN"]()

# sorted_model_file = open("/Users/Stepan/Desktop/models/DBN_sorted", 'r')
first_result_file = open("/Users/Stepan/Desktop/find_bug_2/first_result/result_of_work_from_0.txt", 'w')
second_result_file = open("/Users/Stepan/Desktop/find_bug_2/second_result/result_of_work_from_0.txt", 'w')
third_result_file = open("/Users/Stepan/Desktop/find_bug_2/third_result/result_of_work_from_0.txt", 'w')

zero_real_clicks_counter = 0
query_counter = 0
did_not_find_query_counter = 0
start = time.time()

sessions_iter = SessionsIterator('/Users/Stepan/Desktop/test_data/real_test.txt', 1000)
try:
    for sessions in sessions_iter:
        try:
            for session in sessions:
                query = session.query
                # line = binary_search(sorted_model_file, query)

                # if (line == False):
                #     if (did_not_find_query_counter < 500000):
                #         print("did_not_find_query!!!!!!!")
                #     did_not_find_query_counter += 1
                #     continue
                #
                # _, json_for_query_q = line.split("\t")
                # click_model.from_json(json_for_query_q)

                real_clicks = session.get_clicks()
                # pred_clicks = simulating_user_clicks(click_model, session)


                result = get_result_for(1, real_clicks)
                first_result_file.write(result)

                result = get_result_for(2, real_clicks)
                second_result_file.write(result)

                result = get_result_for(3, real_clicks)
                third_result_file.write(result)

                query_counter += 1

        except Exception as ex:
            template = "An exception of type {0} occured. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print message


except KeyboardInterrupt:
    print 'KeyboardInterrupt'

first_result_file.close()
second_result_file.close()
third_result_file.close()
# sorted_model_file.close()
end = time.time()

print 'results:'
print 'did_not_find_query_counter', did_not_find_query_counter
print 'zero_real_clicks_counter', zero_real_clicks_counter
print 'query_counter', query_counter
print "Work time is", (end - start) / 60, 'min'
print "number of lines passed = ", sessions_iter.line_number
